### Forme (5 points)

- Est ce que le rapport contient des fautes d'orthographes ?
- Est ce que le rapport rappel le titre de l'article étudié ?
- Est ce que les phrases sont bien construites ?
- Est ce que le rapport a été produit avec Latex ? 
- Est ce que les éléments se retrouvent facilement dans le rapport ?
- Est ce que le nom de l'auteur du rapport et les noms des auteurs de l'article sont facile à retrouver.



### Fond (8 points)
Est ce que le rapport contient les éléments suivants ?

- Introduction: Rappel du contexte.
- Rappels du problème: Quel et le problème ? Pourquoi c'est un problème ? Pourquoi ce problème est intéressant ? Que font les autres ? Que propose les auteurs pour résoudre le problème.
- Objectifs du document: Description de la contribution ? Et des résultats ? (Dans le cas d'un survey rappel des catégorisation et des contribution importantes dans chaque catégorie)
- Fidélité à l'article de référence: Est-ce l'idée le rapport est fidèle au document original ? 
    
    

### Fond (5 points)

- Pertinence des idées et des remarques: Est ce que le rapport émet des remarques sur les résultats ? Si oui vous est ce que ces remarques sont pertinentes ?
- Originalité et clarté des propos: En rapport avec le point précédent, est ce que les remarques idée sont originales ? Est ce qu'elles sont présentés correctement ?
- Complétude ("self-contained"): Est ce que le document produit doit faire appel a d'autres sources externes ou est-il complet ?
- Conclusions: Est ce que la conclusion rappel la problématique, et les résultats? 
    


### Aspect subjectif (2 points)

- Est ce que le document rendu est facile à lire ? 
- Est ce que le document rendu permet de saisir et de comprendre l'essence de l'article sans l'avoir lu en détails ?


N'hésitez pas à mettre des remarques pour améliorer le travail de vos camarades. Par exemple: trop long; trop court, pas bien organisé, les phrases sont trop longues, le style d'écriture n'est pas assez ou trop scientifique, il n'est pas nécessaire de reprendre les détails du protocol/équation mais en donner l'idée suffit etc... 
### IMC courses in UNIV-RE.FR

This course is about controlled mobility. There is no handsout nor slides for this course. It 
is more about discussion.

### We are going to developp an algorithm to deploy a fleet of mobile nodes

* List the objectives (what is the goal of our application/algorithm/deployment, ...) This will be partly given during the course
* List the contraints (number, dimension,  ...)
* List assumptions (energy, movements, detection, communication, ...)


Based on the above lists, we are going to desing the algorithm. In order to kickstart
the brainstroming you could think of a:

- Small version of the problem (1d, etc...)
- Discretize the problem (grid, pattern, ...)
- Relax some assumptions (energy, movement, communication...)
- Please do not think centralized, it is a bad idea


### Homework and Lab
 
We will discuss your solution during the class, from theoretical point of view. 
Is it provable ? Is your algorithm correct ? What can we prove ? How to prove it ? 


During the lab, you are going to think about an implementation and how to do it. 
In our case let us think about: How are you going to simulate your algorithm.


You must keep in mind that we you have 2 algorithms. 